/**
 * @description       :
 * @author            : Silvia Velazquez
 * @group             :
 * @last modified on  : 10-15-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log
 * Ver   Date         Author                               Modification
 * 1.0   07-17-2020   William Santana Méndez   Initial Version
 * 1.1   08-21-2020   Ibrahim Napoles          Validate CRUD permission before SOQL/DML operation.
 * 1.2   09-03-2020   Elizabeth Betancourt     Generate and use Consecutive Number to use in File Name.
 **/
public with sharing class XMLresources {

    //formar nombre del xml T(xxxx)aaaammdd
    public static String replaceSomeStr(string strOrigen)
    {
         return strOrigen;
        /* String strOrigenWithReplace = strOrigen.replaceAll('ñ','—');
        strOrigenWithReplace = strOrigenWithReplace.replaceAll('Ñ','—');
        return strOrigenWithReplace;*/
    }
    
    //formar nombre del xml T(xxxx)aaaammdd
    public static String formaNombreXML(List<String> manifiestos)
    {
        if(!(Schema.sObjectType.Manifest__c.isAccessible() &&
             Schema.sObjectType.Manifest__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Manifest__c.fields.Name.isAccessible())) {
            DCException.throwPermiException('XMLresources.formaNombreXML');
        }

        Date todaysDate = system.today();
        String strDay = (todaysDate.day() > 9 ) ? String.valueof(todaysDate.day()) : '0'+ String.valueof(todaysDate.day());
        String strMonth = (todaysDate.month() > 9) ? String.valueof(todaysDate.month()) : '0'+ String.valueof(todaysDate.month());
        String fecha=String.valueof(todaysDate.year())+ strMonth + strDay;
        
        List<Manifest__c> manifiestoList=[select Id, Name from Manifest__c WHERE Id in :manifiestos];
        String nameManifiesto='';

        for (Manifest__c eachManifiesto : manifiestoList) {
            nameManifiesto=eachManifiesto.Name;
        }

        if(nameManifiesto.indexOfIgnoreCase('M') == 0){
            nameManifiesto = nameManifiesto.removeStartIgnoreCase('M');
            nameManifiesto = 'T('+ nameManifiesto + ')';
        }

        String fileName=nameManifiesto+fecha;
        return fileName;

    }
    
    public static String getXMLName( String idManifiesto)
    {
        if(!(Schema.sObjectType.Number_for_XML_File__c.isAccessible() &&
             Schema.sObjectType.Number_for_XML_File__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Number_for_XML_File__c.fields.Name.isAccessible() &&
             Schema.sObjectType.Number_for_XML_File__c.fields.Manifesto__c.isAccessible())) {
            DCException.throwPermiException('XMLresources.getXMLName');
        }

        Date todaysDate = system.today();
        String strDay = (todaysDate.day() > 9 ) ? String.valueof(todaysDate.day()) : '0'+ String.valueof(todaysDate.day());
        String strMonth = (todaysDate.month() > 9) ? String.valueof(todaysDate.month()) : '0'+ String.valueof(todaysDate.month());
        String fecha=String.valueof(todaysDate.year())+ strMonth + strDay;
        
        Number_for_XML_File__c numberXmlFile = new Number_for_XML_File__c( Manifesto__c = idManifiesto );
        List<Number_for_XML_File__c> numberXmlFileList = new List<Number_for_XML_File__c>();
        numberXmlFileList.add(numberXmlFile);
        try {
            //upsert records;
            CRUDEnforce.dmlInsert(numberXmlFileList, 'XMLresources.getXMLName');
        } catch (DmlException e) {
            return '';
        }
        //insert numberXmlFile;
        
        List<Number_for_XML_File__c> listNumbers = [SELECT Id, Name FROM Number_for_XML_File__c Where Id=: numberXmlFile.Id ];
        numberXmlFile = listNumbers[0];
        
        system.debug(numberXmlFile);
        
        String fileNumber='T('+ numberXmlFile.Name + ')';
        String fileName=fileNumber+fecha;
        return fileName;
    }

    //Get procedure number
    public static String procedureNumber(String procedure){
        //TC{YY}{000000}
        /* String cadena=procedure;
        String subCadena = cadena.substring(2); */
        String subCadena = procedure;
        if(procedure.indexOfIgnoreCase('TC') == 0){ //TC{YY}{000000}
            subCadena = procedure.substringAfter('TC');
        }
        else 
            if(procedure.indexOf('-') != -1){  //DCW-12345678
                subCadena = procedure.substringAfter('-'); //12345678
            }
            else{
                subCadena = formatPhone(procedure); //Extracts only numbers
            }

        return subCadena;
    }

    //Cambia formato de fecha yyyy-MM-dd\'T\'HH:mm:ss.ssXXX
    public static String formatFecha(Datetime fecha){
        Datetime fechaSinFormato = fecha;
        String fechaFormateada = fechaSinFormato.format('yyyy-MM-dd\'T\'HH:mm:ss.ssXXX');
        return fechaFormateada;
    }
    /**
     * @desciption Converts Date to a specific format.
     */
    public static String formateaFecha(Date fecha){
        Datetime myDate = Datetime.newInstance(fecha.year(), fecha.month(), fecha.day());
        String result = myDate.format('yyyy-MM-dd\'T\'HH:mm:ss.ssXXX');
        System.debug('Fecha Convertida '+ result);
        return result;
    }

    /**
    * @description Obtain year from a given Date value
    * @author Silvia Velazquez | 08-29-2020 
    * @param fecha 
    * @return String 
    **/
    public static String GetAnno(Date fecha){
        return String.valueOf(fecha.year());
    }

    /**
    * @description Removes non numeric characters from a phone value
    * @author Silvia Velazquez | 08-28-2020 
    * @param phone 
    * @return String 
    **/
    public static String formatPhone(String phoneNumber){
        return phoneNumber.replaceAll('[^0-9]', '');
    }

    //Genera un File y lo almacena en la Org como .Zip
    public static void addFileXML(List<String> manifiestos, String xmlString){
        //String fileName=formaNombreXML(manifiestos); old code
        String fileName=getXMLName(manifiestos[0]);
        
        System.debug('File Name --> '+fileName);
        
        Zippex zip = new Zippex();
        zip.addFile(fileName +'.xml', Blob.valueOf(xmlString), null);
        Blob zipBlob = zip.getZipArchive();
              
        ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S';     // to use S specify this document is in Salesforce, to use E for external files
        conVer.PathOnClient = filename+'.zip';     // The files name, extension is very important here which will help the file in preview.
        conVer.Title = fileName;     // Display name of the files
        conVer.VersionData = zipBlob;     // converting your binary string to Blob
        //insert conVer;        //Insert ContentVersion
        CRUDEnforce.dmlInsert(new List<ContentVersion>{conVer}, 'XMLresources.addFileXML');

        // First get the Content Document Id from ContentVersion Object
        if(!(Schema.sObjectType.ContentVersion.isAccessible() &&
             Schema.sObjectType.ContentVersion.fields.Id.isAccessible() &&
             Schema.sObjectType.ContentVersion.fields.ContentDocumentId.isAccessible())) {
            DCException.throwPermiException('XMLresources.addFileXML');
        }
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
        //create ContentDocumentLink  record
        ContentDocumentLink conDocLink = New ContentDocumentLink();
        conDocLink.LinkedEntityId = manifiestos.get(0);     // Specify RECORD ID here i.e Any Object ID (Standard Object/Custom Object)
        conDocLink.ContentDocumentId = conDoc;      //ContentDocumentId Id from ContentVersion
        conDocLink.shareType = 'V';
        //insert conDocLink;
        CRUDEnforce.dmlInsert(new List<ContentDocumentLink>{conDocLink}, 'XMLresources.addFileXML');
    }

    public static Integer mapMunicipio(String pMunicipio, String pProvincia){
        Map<String, Integer> municipio = new Map<String, Integer>();
        //Artemisa
        municipio.put('Bahia Honda', 686);
        municipio.put('Candelaria', 687);
        municipio.put('San Cristobal', 695);
        municipio.put('Alquizar',700);
        municipio.put('Artemisa',701);
        municipio.put('Bauta', 703);
        municipio.put('Caimito', 705);
        municipio.put('Guanajay', 706);
        municipio.put('Güira de Melena', 708);
        municipio.put('Mariel', 711);
        municipio.put('San Antonio de los Baños', 715);

        //Camagüey
        municipio.put('Carlos Manuel de Cespedes', 784);
        municipio.put('Sierra de Cubitas', 785);
        municipio.put('Esmeralda', 786);
        municipio.put('Florida', 787);
        municipio.put('Guaimaro', 788);
        municipio.put('Jimaguayu', 789);
        municipio.put('Minas', 790);
        municipio.put('Najasa', 791);
        municipio.put('Nuevitas', 792);
        municipio.put('Santa Cruz del Sur', 793);
        municipio.put('Sibanicu', 794);
        municipio.put('Vertientes', 795);
        municipio.put('Camagüey', 847);

        //Ciego de Avila
        municipio.put('Baragua', 774);
        municipio.put('Bolivia', 775);
        municipio.put('Ciego de Avila', 776);
        municipio.put('Ciro Redondo', 777);
        municipio.put('Chambas', 778);
        municipio.put('Florencia', 779);
        municipio.put('Majagua', 780);
        municipio.put('Moron', 781);
        municipio.put('Venezuela', 782);
        municipio.put('Primero de Enero', 783);

        //Cienfuegos
        municipio.put('Abreus', 758);
        municipio.put('Aguada de Pasajeros', 759);
        municipio.put('Cienfuegos', 760);
        municipio.put('Cumanayagua', 761);
        municipio.put('Cruces', 762);
        municipio.put('Lajas', 763);
        municipio.put('Palmira', 764);
        municipio.put('Rodas', 765);

        //Granma
        municipio.put('Bartolome Maso', 816);
        municipio.put('Bayamo', 817);
        municipio.put('Buey Arriba', 818);
        municipio.put('Campechuela', 819);
        municipio.put('Cauto Cristo', 820);
        municipio.put('Guisa', 821);
        municipio.put('Jiguani', 822);
        municipio.put('Manzanillo', 823);
        municipio.put('Media Luna', 824);
        municipio.put('Niquero', 825);
        municipio.put('Pilon', 826);
        municipio.put('Rio Cauto', 827);
        municipio.put('Yara', 828);

        //Guantánamo
        municipio.put('Baracoa', 838);
        municipio.put('Caimanera', 839);
        municipio.put('El Salvador', 840);
        municipio.put('Imias', 841);
        municipio.put('Maisi', 842);
        municipio.put('Manuel Tames', 843);
        municipio.put('Niceto Perez', 844);
        municipio.put('San Antonio del Sur', 845);
        municipio.put('Yateras', 846);
        municipio.put('Guantanamo', 849);

        //Holguín
        municipio.put('Antilla', 803);
        municipio.put('Banes', 804);
        municipio.put('Baguanos', 805);
        municipio.put('Cacocum', 806);
        municipio.put('Calixto Garcia', 807);
        municipio.put('Cueto', 808);
        municipio.put('Frank Pais', 809);
        municipio.put('Gibara', 810);
        municipio.put('Mayari', 811);
        municipio.put('Moa', 812);
        municipio.put('Rafael Freyre', 813);
        municipio.put('Sagua de Tanamo', 814);
        municipio.put('Holguin', 848);

        if(pProvincia=='Holguin')
            municipio.put('Urbano Noris', 815);

        if (pProvincia=='Isla de la Juventud')
            municipio.put('Urbano Noris', 518);

        //La Habana
        municipio.put('Arroyo Naranjo', 719);
        municipio.put('Centro Habana', 720);
        municipio.put('La Habana Vieja', 721);
        municipio.put('Cerro', 722);
        municipio.put('Cotorro', 723);
        municipio.put('Diez de Octubre', 724);
        municipio.put('Guanabacoa', 725);
        municipio.put('La Habana del Este', 726);
        municipio.put('La Lisa', 727);
        municipio.put('Marianao', 728);
        municipio.put('Playa', 729);
        municipio.put('Regla', 730);
        municipio.put('San Miguel del Padron', 731);
        municipio.put('Boyeros', 732);
        municipio.put('Plaza de la Revolucion', 733);

        //Las Tunas
        municipio.put('Amancio', 796);
        municipio.put('Colombia', 797);
        municipio.put('Jobabo', 798);
        municipio.put('Majibacoa', 799);
        municipio.put('Manati', 800);
        municipio.put('Jesus Menendez', 801);
        municipio.put('Puerto Padre', 802);
        municipio.put('Las Tunas', 853);

        //Matanzas
        municipio.put('Calimete', 734);
        municipio.put('Cardenas', 735);
        municipio.put('Cienaga de Zapata', 736);
        municipio.put('Colon', 737);
        municipio.put('Los Arabos', 738);  //Aqui modificar de acuerdo a la interfaz: Los Arabos
        municipio.put('Marti', 739);
        municipio.put('Matanzas', 740);
        municipio.put('Pedro Betancourt', 741);
        municipio.put('Perico', 742);
        municipio.put('Union de Reyes', 743);
        municipio.put('Varadero', 744);
        municipio.put('Jagüey Grande', 850);
        municipio.put('Jovellanos', 851);
        municipio.put('Limonar', 852);

        //Mayabeque
        municipio.put('Batabano', 702);
        municipio.put('Bejucal', 704);
        municipio.put('Güines', 707);
        municipio.put('Jaruco', 709);
        municipio.put('Madruga', 710);
        municipio.put('Melena del Sur', 712);
        municipio.put('Nueva Paz', 713);
        municipio.put('Quivican', 714);
        municipio.put('San Jose de las Lajas', 716);
        municipio.put('San Nicolas', 717);
        municipio.put('Santa Cruz del Norte', 718);

        //Pinar del Rio
        municipio.put('Consolacion del Sur',688);
        municipio.put('Guane', 689);
        municipio.put('La Palma', 690);
        municipio.put('Los Palacios', 691);
        municipio.put('Mantua', 692);
        municipio.put('Minas de Matahambre', 693);
        municipio.put('Pinar del Rio', 694);
        municipio.put('Sandino', 696);
        municipio.put('San Juan y Martinez', 697);
        municipio.put('Viñales', 699);

        if(pProvincia=='Pinar del Rio')
            municipio.put('San Luis', 698);

        if (pProvincia=='Santiago de Cuba')
            municipio.put('San Luis', 834);

        //Santiago de Cuba
        municipio.put('Contramaestre', 829);
        municipio.put('Guama', 830);
        municipio.put('Songo - La Maya', 831);
        municipio.put('Mella', 832);
        municipio.put('Palma Soriano', 833);
        municipio.put('Santiago de Cuba', 835);
        municipio.put('Segundo Frente', 836);
        municipio.put('Tercer Frente', 837);

        //Sancti Spiritus
        municipio.put('Cabaiguan', 766);
        municipio.put('Fomento', 767);
        municipio.put('Jatibonico', 768);
        municipio.put('La Sierpe', 769);
        municipio.put('Sancti Spiritus', 770);
        municipio.put('Taguasco', 771);
        municipio.put('Trinidad', 772);
        municipio.put('Yaguajay', 773);

        //Villa Clara
        municipio.put('Caibarien', 745);
        municipio.put('Camajuani', 746);
        municipio.put('Cifuentes', 747);
        municipio.put('Corralillo', 748);
        municipio.put('Encrucijada', 749);
        municipio.put('Manicaragua', 750);
        municipio.put('Placetas', 751);
        municipio.put('Quemado de Güines', 752);
        municipio.put('Ranchuelo', 753);
        municipio.put('Remedios', 754);
        municipio.put('Sagua la Grande', 755);
        municipio.put('Santa Clara', 756);
        municipio.put('Santo Domingo', 757);

        Integer valorMunicipio=-1;
        for (String key : municipio.keySet()) {
            if(municipio.containsKey(pMunicipio)) {
                valorMunicipio=municipio.get(pMunicipio);
            }
        }
        return valorMunicipio;
    }

    public static Integer mapProvincia(String pProvincia){
        Map<String, Integer> provincia = new Map<String, Integer>();
        provincia.put('Pinar del Rio', 671);
        provincia.put('Mayabeque', 672);
        provincia.put('La Habana', 673);
        provincia.put('Matanzas', 674);
        provincia.put('Villa Clara', 675);
        provincia.put('Cienfuegos', 676);
        provincia.put('Sancti Spiritus', 677);
        provincia.put('Ciego de Avila', 678);
        provincia.put('Camagüey', 679);
        provincia.put('Las Tunas', 680);
        provincia.put('Holguin', 681);
        provincia.put('Granma', 682);
        provincia.put('Santiago de Cuba', 683);
        provincia.put('Guantanamo', 684);
        provincia.put('Isla de la Juventud', 685);
        provincia.put('Artemisa', 686);

        Integer valorProvincia=-1;
        for (String key : provincia.keySet()) {
            if(provincia.containsKey(pProvincia)) {
                valorprovincia=provincia.get(pProvincia);
            }
        }
        return valorprovincia;
    }

    public static Integer mapPais(String pPais){
        Map<String, Integer> pais = new Map<String, Integer>();
        pais.put('Cuba', 1);
        pais.put('Guayana Francesa', 5);
        pais.put('Guinea', 6);
        pais.put('Guinea Bissau', 7);
        pais.put('Guinea Ecuatorial', 8);
        pais.put('Guyana', 9);
        pais.put('Haiti', 10);
        pais.put('Honduras', 11);
        pais.put('Hong Kong', 12);
        pais.put('Hungría', 13);
        pais.put('India', 14);
        pais.put('Indonesia', 15);
        pais.put('Irán', 16);
        pais.put('Irak', 17);
        pais.put('Irlanda', 18);
        pais.put('Bouvet', 19);
        pais.put('Isla Christmas',20);
        pais.put('Isla Norfolk', 21);
        pais.put('Islandia', 22);
        pais.put('Islas Caimán', 23);
        pais.put('Cocos (Keeling)', 24);
        pais.put('Islas Cook', 25);
        pais.put('Islas Feroe', 26);
        pais.put('Islas Heard y McDonald', 27);
        pais.put('Islas Malvinas', 28);
        pais.put('Islas Marianas Septentrionales', 29);
        pais.put('Islas Marshall', 30);
        pais.put('Islas Remotas Menores de los Estados Unidos', 31);
        pais.put('Islas Salomón', 32);
        pais.put('Islas Svalbard y Yan Mayen', 33);
        pais.put('Islas Turcas y Caicos', 34);
        pais.put('Islas Virgenes Británicas', 35);
        pais.put('Islas Virgenes Americanas', 36);
        pais.put('Islas Wallis y Futuna', 37);
        pais.put('Israel', 38);
        pais.put('Italia', 39);
        pais.put('Jamahiriya Arabe libya', 40);
        pais.put('Jamaica', 41);
        pais.put('Japón', 42);
        pais.put('Jordania', 43);
        pais.put('Kazajstán', 44);
        pais.put('Kenya', 45);
        pais.put('Kirguistán', 46);
        pais.put('Kiribati', 47);
        pais.put('Kuwait', 48);
        pais.put('Ex Republica Yugoslavia de Macedonia', 49);
        pais.put('Lesotho', 50);
        pais.put('Letonia', 51);
        pais.put('Líbano', 52);
        pais.put('Liberia', 53);
        pais.put('Liechtenstein', 54);
        pais.put('Lituania', 55);
        pais.put('Luxemburgo', 56);
        pais.put('Macao, Region Administrativa Especial de China', 57);
        pais.put('Madagascar', 58);
        pais.put('Malasia', 59);
        pais.put('Malawi', 60);
        pais.put('Maldivas', 61);
        pais.put('Mali', 62);
        pais.put('Malta', 63);
        pais.put('Marruecos', 64);
        pais.put('Martinica', 65);
        pais.put('Mauricio', 66);
        pais.put('Mauritania', 67);
        pais.put('Mayotte', 68);
        pais.put('México', 69);
        pais.put('Micronesia, Estados Federados de', 70);
        pais.put('Monaco', 71);
        pais.put('Mongolia', 72);
        pais.put('Montserrat', 73);
        pais.put('Mozambique', 74);
        pais.put('Mayanmar', 75);
        pais.put('Namibia', 76);
        pais.put('Nauru', 77);
        pais.put('Nepal', 78);
        pais.put('Nicaragua', 79);
        pais.put('Níger', 80);
        pais.put('Nigeria', 81);
        pais.put('Niue', 82);
        pais.put('Noruega', 83);
        pais.put('Nueva Caledonia', 84);
        pais.put('Nueva Zelandia', 85);
        pais.put('Omán', 86);
        pais.put('Afganistán', 87);
        pais.put('Albania', 88);
        pais.put('Alemania', 89);
        pais.put('Andorra', 90);
        pais.put('Angola', 91);
        pais.put('Anguila', 92);
        pais.put('Antartida', 93);
        pais.put('Antigua y Barbuda', 94);
        pais.put('Antillas Holandesas', 95);
        pais.put('Arabia Saudita', 96);
        pais.put('Argelia', 97);
        pais.put('Argentina', 98);
        pais.put('Armenia', 99);
        pais.put('Aruba', 100);
        pais.put('Australia', 101);
        pais.put('Austria', 102);
        pais.put('Azerbaiyán', 103);
        pais.put('Bahamas', 104);
        pais.put('Baréin', 105);
        pais.put('Bangladesh', 106);
        pais.put('Barbados', 107);
        pais.put('Belarus', 108);
        pais.put('Bélgica', 109);
        pais.put('Belice', 110);
        pais.put('Benín', 111);
        pais.put('Bermudas', 112);
        pais.put('Bhutan', 113);
        pais.put('Bolivia', 114);
        pais.put('Bosnia-Herzegovina', 115);
        pais.put('Botsuana', 116);
        pais.put('Brasil', 117);
        pais.put('Brunei Darussalam', 118);
        pais.put('Bulgaria', 119);
        pais.put('Burkina Faso', 120);
        pais.put('Burundi', 121);
        pais.put('Cabo Verde', 122);
        pais.put('Camboya', 123);
        pais.put('Camerún', 124);
        pais.put('Canadá', 125);
        pais.put('Chad', 126);
        pais.put('Chile', 127);
        pais.put('China', 128);
        pais.put('Chipre', 129);
        pais.put('Colombia', 130);
        pais.put('Comoras', 131);
        pais.put('Congo', 132);
        pais.put('Costa Rica', 133);
        pais.put('Cote D Ivoire', 134);
        pais.put('Croacia', 135);
        pais.put('Guatemala', 136);
        pais.put('Dinamarca', 137);
        pais.put('Djibouti', 138);
        pais.put('Dominica', 139);
        pais.put('Ecuador', 140);
        pais.put('Egipto', 141);
        pais.put('El Salvador', 142);
        pais.put('Emiratos Árabes Unidos', 143);
        pais.put('Eslovaquia', 144);
        pais.put('Eritrea', 145);
        pais.put('Eslovenia', 146);
        pais.put('España', 147);
        pais.put('Santa Sede, Vaticano, Ciudad del Vaticano', 148);
        pais.put('US', 149);
        pais.put('Estonia', 150);
        pais.put('Etiopía', 151);
        pais.put('Federación Rusa', 152);
        pais.put('Fiji', 153);
        pais.put('Filipinas', 154);
        pais.put('Finlandia', 155);
        pais.put('Francia', 156);
        pais.put('Francia', 157);
        pais.put('Gabón', 158);
        pais.put('Gambia', 159);
        pais.put('Georgia', 160);
        pais.put('Ghana', 161);
        pais.put('Gibraltar', 162);
        pais.put('Granada', 163);
        pais.put('Grecia', 164);
        pais.put('Groenlandia', 165);
        pais.put('Guadalupe', 166);
        pais.put('Guinea Conakry', 167);
        pais.put('Países Bajos', 168);
        pais.put('Pakistán', 169);
        pais.put('Palau', 170);
        pais.put('Panamá', 171);
        pais.put('Papúa-Nueva Guinea', 172);
        pais.put('Paraguay', 173);
        pais.put('Perú', 174);
        pais.put('Pitcairn', 175);
        pais.put('Polinesia Francesa', 176);
        pais.put('Polonia', 177);
        pais.put('Portugal', 178);
        pais.put('Puerto Rico', 179);
        pais.put('Qatar', 180);
        pais.put('Reino Unido', 181);
        pais.put('Siria', 187);
        pais.put('República Checa', 189);
        pais.put('República Democrática del Congo', 191);
        pais.put('Laos', 192);
        pais.put('Dominicana', 194);
        pais.put('República Popular Democrática del Congo', 195);
        pais.put('Tanzania', 196);
        pais.put('Reunion', 197);
        pais.put('Rumania', 198);
        pais.put('Rwanda', 199);
        pais.put('Sahara Occidental', 200);
        pais.put('Saint Kitts y Nevis', 201);
        pais.put('Samoa', 202);
        pais.put('Samoa Estadounidense', 203);
        pais.put('San Marino', 204);
        pais.put('San Vicente y las Granadinas', 205);
        pais.put('Santa Helena', 206);
        pais.put('Santa Lucía', 207);
        pais.put('Santo Tomé y Príncipe', 208);
        pais.put('Senegal', 209);
        pais.put('Seychelles', 210);
        pais.put('Sierra Leona', 211);
        pais.put('Singapur', 212);
        pais.put('Somalia', 213);
        pais.put('Sri Lanka', 214);
        pais.put('St Pierre y Miquelon', 215);
        pais.put('Sudáfrica', 216);
        pais.put('Sudán', 217);
        pais.put('Suecia', 218);
        pais.put('Suiza', 219);
        pais.put('Surinam', 220);
        pais.put('Swazilandia', 221);
        pais.put('Tailandia', 222);
        pais.put('Taiwan', 223);
        pais.put('Tayikistán', 224);
        pais.put('Timor Oriental', 228);
        pais.put('Togo', 229);
        pais.put('Tokelau', 230);
        pais.put('Tonga', 231);
        pais.put('Trinidad y Tobago', 232);
        pais.put('Túnez', 233);
        pais.put('Turkmenistán', 234);
        pais.put('Turquía', 235);
        pais.put('Tuvalu', 236);
        pais.put('Ucrania', 237);
        pais.put('Uganda', 238);
        pais.put('Uruguay', 239);
        pais.put('Uzbekistán', 240);
        pais.put('Vanuatu', 241);
        pais.put('Venezuela', 242);
        pais.put('Vietnam', 243);
        pais.put('Yemen', 244);
        pais.put('Serbia', 245);
        pais.put('Zambia', 246);
        pais.put('Zimbabwe', 247);
        pais.put('Territorio Británico Indico', 248);
        pais.put('República Centroafricana', 249);
        pais.put('Guam', 250);
        pais.put('Moldavia', 251);
        pais.put('Georgia/ Sandwich del Sur', 252);
        pais.put('Corea Sur', 253);
        pais.put('Palestina', 254);
        pais.put('Libia', 255);

        Integer valorPais=-1;
        for (String key : pais.keySet()) {
            if(pais.containsKey(pPais)) {
                valorpais=pais.get(pPais);
            }
        }

        return valorPais;
    }

    /**
    * @description Maps State with NUmeric Code
    * @author Silvia Velazquez | 08-29-2020 
    * @param pEstado 
    * @return Integer 
    **/
    public static Integer mapEstado(String pEstado){
        Map<String, Integer> estado = new Map<String, Integer>();
        estado.put('Alabama',2517);  
        estado.put('AL',2517);
        estado.put('Alaska',2518);  
        estado.put('AK',2518);
        estado.put('Arizona',2519);  
        estado.put('AZ',2519);
        estado.put('Arkansas',2520);  
        estado.put('AR',2520);
        estado.put('California',2521);  
        estado.put('CA',2521);
        estado.put('Colorado',2522); 
        estado.put('CO',2522);
        estado.put('Connecticut',2523);  
        estado.put('CT',2523);
        estado.put('Conyers',2524);
        estado.put('DC',2525);
        estado.put('Delaware',2526);  
        estado.put('DE',2526);
        estado.put('Florida',2527);  
        estado.put('FL',2527);
        estado.put('Georgia',2528);  
        estado.put('GA',2528);
        estado.put('Hawaii',2529);  
        estado.put('HI',2529);
        estado.put('Idaho',2530);  
        estado.put('ID',2530);
        estado.put('Illinois',2531);  
        estado.put('IL',2531);
        estado.put('Indiana',2532);  
        estado.put('IN',2532);
        estado.put('Iowa',2533);  
        estado.put('IA',2533);
        estado.put('Kansas',2534);  
        estado.put('KS',2534);
        estado.put('Kentucky',2535); 
        estado.put('KY',2535);
        estado.put('Louisiana',2536);  
        estado.put('LA',2536);
        estado.put('Maine',2537);  
        estado.put('ME',2537);
        estado.put('Maryland',2538);  
        estado.put('MD',2538);
        estado.put('Massachusetts',2539);  
        estado.put('MA',2539);
        estado.put('Michigan',2540);  
        estado.put('MI',2540);
        estado.put('Minesota',2541);  
        estado.put('MN',2541);
        estado.put('Mississippi',2542);  
        estado.put('MS',2542);
        estado.put('Missouri',2543);  
        estado.put('MO',2543);
        estado.put('Montana',2544);  
        estado.put('MT',2544);
        estado.put('Nebraska',2545);  
        estado.put('NE',2545);
        estado.put('Nevada',2546);  
        estado.put('NV',2546);
        estado.put('New Hampshire',2547);  
        estado.put('NH',2547);
        estado.put('New Jersey',2548);  
        estado.put('NJ',2548);
        estado.put('New Mexico',2549);  
        estado.put('NM',2549);
        estado.put('New York',2550);  
        estado.put('NY',2550);
        estado.put('North Carolina',2551);  
        estado.put('NC',2551);
        estado.put('North Dakota',2552);  
        estado.put('ND',2552);
        estado.put('Ohio',2553);  
        estado.put('OH',2553);
        estado.put('Oklahoma',2554);  
        estado.put('OK',2554);
        estado.put('Oregon',2555);  
        estado.put('OR',2555);
        estado.put('Pennsylvania',2556);  
        estado.put('PA',2556);
        estado.put('Puerto Rico',2557);
        estado.put('Rhode Island',2558);  
        estado.put('RI',2558);
        estado.put('South Carolina',2559);  
        estado.put('SC',2559);
        estado.put('South Dakota',2560);  
        estado.put('SD',2560);
        estado.put('Tennessee',2561);  
        estado.put('TN',2561);
        estado.put('Texas',2562);  
        estado.put('TX',2562);
        estado.put('Utah',2563);  
        estado.put('UT',2563);
        estado.put('Vermont',2564);  
        estado.put('VT',2564);
        estado.put('Virgin Island',2565);
        estado.put('Virginia',2566);  
        estado.put('VA',2566);
        estado.put('Washington',2567);  
        estado.put('WA',2567);
        estado.put('West Virginia',2568);  
        estado.put('WV',2568);
        estado.put('Wisconsin',2569);  
        estado.put('WI',2569);
        estado.put('Wyoming',2570);  
        estado.put('WY',2570);

        Integer valorEstado=-1;
            if(estado.containsKey(pEstado)) {
                valorEstado = estado.get(pEstado);
            }

        return valorEstado;
    }
}