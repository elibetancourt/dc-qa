/**
 * FingerprintCaptur controller
 * @version 0.1
 * @author Fernando Gomez
 */
global class PassportPictureCtrl {

	/**
	 * Saves the passport picture url
	 * @param recordId
	 * @param documentId
	 */
	@AuraEnabled
	global static String getPassportPictureUrl(Id recordId) {
		try {
			return [
				SELECT PassportPictureURL__c
				FROM Procedure__c
				WHERE Id = :recordId
			].PassportPictureURL__c;
		} catch (Exception ex) {
			throw new AuraException(ex.getMessage());
		}
	}
	
	/**
	 * Saves the passport picture url
	 * @param recordId
	 * @param documentId
	 */
	@AuraEnabled
	global static String savePassportPictureUrl(Id recordId, Id documentId) {
		Procedure__c t;
		ContentVersion v;
		try {
			t = [
				SELECT Id
				FROM Procedure__c
				WHERE Id = :recordId
			];

			v = [
				SELECT ContentDocument.title 
				FROM ContentVersion 
				WHERE ContentDocumentId = :documentId
				AND IsLatest = true
				LIMIT 1
			];

			t.PassportPictureURL__c = 
				// 'https://spdistrictcuba--c.documentforce.com' +
				'/sfc/servlet.shepherd/version/renditionDownload?' +
				'rendition=ORIGINAL_Png&versionId=' + v.Id;
			update t;
			return t.PassportPictureURL__c;
		} catch (Exception ex) {
			throw new AuraException(ex.getMessage());
		}
	}
}