/**
 * @File Name          : dcConvertCurrencyToWords.cls
 * @Description        : Class with an Invocable Method for Number to Words conversion.
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 11/8/2020 5:06:21 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/8/2020   Silvia Velazquez     Initial Version
**/
public with sharing class dcConvertCurrencyToWords {

    @InvocableMethod(label='Generate Number to Words' description='Generate Literal Description for a Currency Amount')
    public static List<OutputParam> convertToWords (List<InputParam> requestList) {
        List<OutputParam> results = new List<OutputParam>();
        for(InputParam req: requestList){
            OutputParam res = new OutputParam();
            //res.amount = req.amount;
            res.description = NumberTOWordConvertion.CurrencyToWordsFormat(req.amount);
            results.add(res);
        }

        return results;    
    }

    public class InputParam {
        @InvocableVariable(label='Records for Input' required=true)
        public Decimal amount;
    }

    public class OutputParam {
        /* @InvocableVariable(label='Amount for Output' required=true)
        public Decimal amount; */

        @InvocableVariable(label='Amount Description for Output' required=true)
        public String description;
    }

}