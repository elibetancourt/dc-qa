/**
 * @description       : 
 * @author            : Silvia Velazquez
 * @group             : 
 * @last modified on  : 09-01-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   08-26-2020   Silvia Velazquez   Initial Version
**/
@isTest
private class dcApplyPaymentUtilityTest {
    @testSetup 
    static void makeTestData(){
        Map<String,String> tramite = TestDataFactory.getTramiteParams(true, true, true);
        TestDataFactory.generateArticuloLote(tramite);
    }

    @isTest
    static void getBatchDataOK(){
        Test.startTest();
        Batch__c batch = [SELECT Id FROM Batch__c LIMIT 1];
        dcApplyPaymentController.Batch_Wrapper result = dcApplyPaymentUtility.getBatchData(batch.Id);
        System.debug('Result: '+result);
        System.assertNotEquals(null, result,'');
        Test.stopTest();
    }

    @isTest
    static void getBatchNoData(){
        Test.startTest();
        Batch__c batch = [SELECT Id FROM Batch__c LIMIT 1];
        String batchId = batch.Id;
        delete batch;
        dcApplyPaymentController.Batch_Wrapper result = dcApplyPaymentUtility.getBatchData(batchId);
        System.assertEquals(null, result,'');
        Test.stopTest();
    }

    @isTest
    static void applyPaymentOK(){
        Test.startTest();
        dcApplyPaymentController.ItemsToApply_Wrapper items = new dcApplyPaymentController.ItemsToApply_Wrapper();
        Batch__c batch = [SELECT Id FROM Batch__c LIMIT 1];
        items.batchId = batch.Id;
        Batch_Item__c batchItem = [SELECT Id FROM Batch_Item__c WHERE Batch__c = :batch.Id LIMIT 1];
        items.itemsToApply = new List<String>{batchItem.Id};
        String result = dcApplyPaymentUtility.applyPayment(items);
        System.assertEquals('OK', result,'');
        Test.stopTest();
    }

    @isTest
    static void applyPaymentWithoutItems(){
        Test.startTest();
        dcApplyPaymentController.ItemsToApply_Wrapper items = new dcApplyPaymentController.ItemsToApply_Wrapper();
        Batch__c batch = [SELECT Id FROM Batch__c LIMIT 1];
        items.batchId = batch.Id;
        items.itemsToApply = new List<String>();
        String result = dcApplyPaymentUtility.applyPayment(items);
        System.assertNotEquals('OK', result,'');
        Test.stopTest();
    }

    @isTest
    static void applyPaymentInvalidId(){
        Test.startTest();
        dcApplyPaymentController.ItemsToApply_Wrapper items = new dcApplyPaymentController.ItemsToApply_Wrapper();
        Batch__c batch = [SELECT Id FROM Batch__c LIMIT 1];
        items.batchId = batch.Id;
        delete batch;
        items.itemsToApply = new List<String>();
        String result = dcApplyPaymentUtility.applyPayment(items);
        System.assertNotEquals('OK', result,'');
        Test.stopTest();
    }
}