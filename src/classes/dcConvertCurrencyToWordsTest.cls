/**
 * @File Name          : dcConvertCurrencyToWordsTest.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 08-26-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/8/2020   Silvia Velazquez     Initial Version
**/

@isTest
private class dcConvertCurrencyToWordsTest {

    static List<dcConvertCurrencyToWords.InputParam> makeInputData(){
        List<dcConvertCurrencyToWords.InputParam> requestList = new List<dcConvertCurrencyToWords.InputParam>();
        Decimal item = 54.0;
        for(Integer i=0;i < 3 ; i++){
            dcConvertCurrencyToWords.InputParam req = new dcConvertCurrencyToWords.InputParam();
            req.amount = item.pow(i);
            Integer n = i+1;
            System.debug('Number'+n+': '+req.amount);
            requestList.add(req);
        }
        return requestList;
    }


    @isTest
    static void convertNumberToWordsTest(){
        List<dcConvertCurrencyToWords.InputParam> requestList = makeInputData();
        List<dcConvertCurrencyToWords.OutputParam> results = dcConvertCurrencyToWords.convertToWords(requestList);
        Integer i = 0;
        for(dcConvertCurrencyToWords.OutputParam res: results){
            i++;
            System.debug('Result'+i+': '+res.description);
        }
        System.assert(results.size() > 0, '');
    }

}