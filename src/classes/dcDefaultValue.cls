public class dcDefaultValue {
         @AuraEnabled
         public Map<string,string> defaultMapValues {get; set;}
         
         public dcDefaultValue(Map<string,string> defaultMapValues){
             this.defaultMapValues=defaultMapValues;
         }
}