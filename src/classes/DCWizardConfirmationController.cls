/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 10-15-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author             Modification
 * 1.0   10-15-2020   Silvia Velazquez   Initial Version
**/
global without sharing class DCWizardConfirmationController {
    public DCWizardConfirmationController() {

    }
}