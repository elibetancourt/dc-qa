/**
 * @File Name          : dcSearchUtility.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Elizabeth Betancourt Herrera
 * @Last Modified On   : 10-15-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    27/7/2020   Silvia Velazquez     Initial Version
 * 1.1   08-21-2020   Ibrahim Napoles          Validate CRUD permission before SOQL/DML operation.
**/
public with sharing class dcSearchUtility {
    public static dcSearchContactResult contactResult {get; set;}
    public static String contactFields = 'Id,FirstName, Middle_Name__c, LastName, Second_Last_Name__c, Gender__c, Birthdate';
    public static String procedureFields = 'Id,Name,RecordType.Name,Status__c,Agency__r.Name,CreatedDate';
    public static String procedureWhere = 'Status__c != \'Entregado\' AND Status__c !=\'Cancelado\' AND CreatedDate = LAST_N_DAYS:';


    public static dcSearchContactResult count(dcSearchContact searchPattern){
        //check accesibility
        checkAccessible();
        contactResult = search(searchPattern);
        String query = generateQuery(searchPattern, false); 
        List<Contact> listContact = Database.query(query);
        Integer totalResults = 0;
        if(listContact.size() > 0){            
            for(Contact c:listContact){
                for(Procedure__c p: c.Procedures__r){
                   totalResults ++;
                }
            } 
        }
        contactResult.count = totalResults;
        return contactResult;
    }
    
    public static dcSearchContactResult search(dcSearchContact searchPattern){
        
        //check accesibility
        checkAccessible();
        String query = generateQuery(searchPattern, true);     
        System.debug(query);
        List<Contact> listContact = Database.query(query);

        List<dcContactProcedureSearch> resultList = new List<dcContactProcedureSearch>();
        if(listContact.size() > 0){            
            for(Contact c:listContact){
                for(Procedure__c p: c.Procedures__r){
                    dcContactProcedureSearch item = new dcContactProcedureSearch();
                    item.contactId = c.Id;
                    item.firstName = c.FirstName;
                    item.middleName = c.Middle_Name__c;
                    item.lastName = c.LastName;
                    item.secondLastName = c.Second_Last_Name__c;
                    item.gender = c.Gender__c;
                    item.birthdate = c.Birthdate;
                    item.procedureId = p.Id;
                    item.procedureName = p.Name;
                    item.procedureType = p.RecordType.Name;
                    item.procedureStatus = p.Status__c;
                    item.consuladoStatus = !item.procedureStatus.containsIgnoreCase('Consulado');
                    item.procedureAgency = p.Agency__r.Name;
                    String strDatetime = p.CreatedDate.format('MM/dd/yyyy HH:mm');
                    //Datetime formattedDate = (DateTime)JSON.deserialize('"' + strDatetime + '"', DateTime.class);
                    item.procedureDate = strDatetime;
                    item.procedureNameLink = '/'+item.procedureId;
                    resultList.add(item);
                }
            } 
        }
    
        contactResult = new dcSearchContactResult(resultList,searchPattern);
        return contactResult;
    }
    
    
    public static dcSearchContactResult reset(dcSearchContact searchPattern){
        contactResult = new dcSearchContactResult(searchPattern);
    
        return contactResult;
    }

    public static Boolean updateProcedure(String contactId, String procedureId, String passportNumber,Date expirationDate){
        if(!(Schema.sObjectType.Contact.isAccessible() &&
             Schema.sObjectType.Contact.fields.Id.isAccessible() &&
             Schema.sObjectType.Contact.fields.Passport_Number__c.isAccessible() &&
             Schema.sObjectType.Contact.fields.Passport_Expiration_Date__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Customer__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Name.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.RecordTypeId.isAccessible() &&
             Schema.sObjectType.RecordType.isAccessible() &&
             Schema.sObjectType.RecordType.fields.Name.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Status__c.isAccessible())) { 
            DCException.throwPermiException('dcSearchUtility.updateProcedure');
        }

        Boolean result;
        List<Contact> listContact =  [SELECT Passport_Number__c,Passport_Expiration_Date__c,
                                     (SELECT Id,Name,RecordType.Name,Status__c FROM Procedures__r WHERE Id = :procedureId)
                                        FROM Contact  WHERE Id = :contactId];
        if(listContact.size() > 0){
            Contact c = listContact[0];
            Integer validYears = getValidPassportYears();
            if(c.Procedures__r.size() > 0){
                Procedure__c p = c.Procedures__r[0];
                c.Passport_Number__c = passportNumber;
                c.Passport_Expiration_Date__c = expirationDate;

                String valueDebug = 'Expiration Date: ' + c.Passport_Expiration_Date__c;
                System.debug(valueDebug);
                
                c.Expired_Passport_Issue_Date__c = expirationDate.addYears(validYears * -1);
                //update c;
                CRUDEnforce.dmlUpdate(new List<Contact>{c}, 'dcSearchUtility.updateProcedure');

                p.Status__c = 'Completado';
                p.Numero_de_Pasaporte__c = passportNumber;
                p.Passport_Expiration_Date__c = expirationDate;
                p.Expired_Passport_Issue_Date__c = expirationDate.addYears(validYears * -1);
                //update p;
                CRUDEnforce.dmlUpdate(new List<Procedure__c>{p}, 'dcSearchUtility.updateProcedure');
                result = true;
            }
            else { result = false;}  
        }
        else {result = false;}

        return result;
    }

    /* public static Date getSearchDateLimit(){
        List<Search_Limit_In_Days__mdt> searchSettings = [SELECT Days__c FROM Search_Limit_In_Days__mdt WHERE MasterLabel = 'Days_Limit'];
        Integer days = 15;
        if(searchSettings.size() > 0) {
            days = (Integer) searchSettings[0].Days__c;
        }
        System.debug('Fecha Limite: '+ String.valueOf(Date.today().addDays(days * -1)));
        return Date.today().addDays(days * -1); // Getting the Minimun date to use in contacts search 
    } */

    public static Integer getSearchLimit(){
        if(!(Schema.sObjectType.Search_Limit_In_Days__mdt.isAccessible() &&
            Schema.sObjectType.Search_Limit_In_Days__mdt.fields.MasterLabel.isAccessible() &&
            Schema.sObjectType.Search_Limit_In_Days__mdt.fields.Days__c.isAccessible())) {
        DCException.throwPermiException('dcSearchUtility.getSearchLimit');
        }
        List<Search_Limit_In_Days__mdt> searchSettings = [SELECT Days__c FROM Search_Limit_In_Days__mdt WHERE MasterLabel = 'Days_Limit'];
        Integer days = 15;
        if(searchSettings.size() > 0) {
            days = (Integer) searchSettings[0].Days__c;
        }
        return days;
    }

    public static String getContactsWhere(dcSearchContact searchPattern,Boolean withLimit){
        //Date birthdate = searchPattern.birthdate;
        String formattedDate = getDateFormat(searchPattern.birthdate);
        String result = 'FirstName = \''+searchPattern.firstName +'\' AND LastName = \''+searchPattern.lastName+'\' AND Second_Last_Name__c = \''+searchPattern.secondLastName+'\'';
               result += ' AND Birthdate ='+ formattedDate +' AND Middle_Name__c = \''+ searchPattern.middleName +'\'';
      
        if(withLimit == true){
            result += ' LIMIT '+searchPattern.pageSize+' OFFSET '+searchPattern.offset;
        }

        return result;
        /* return 'FirstName = \'searchPattern.firstName\' AND LastName = \'searchPattern.lastName\' AND Second_Last_Name__c = \'searchPattern.secondLastName\''
                        + ' AND Birthdate = searchPattern.birthdate AND Middle_Name__c = \'searchPattern.middleName\' LIMIT searchPattern.pageSize OFFSET searchPattern.offset'; */
    }

    public static String getOrientationOrder(dcSearchContact searchPattern){
        return (searchPattern.isDescending == true)? 'DESC': 'ASC';
    }

    public static String generateQuery(dcSearchContact searchPattern,Boolean withLimit){
        Integer days = getSearchLimit();
        String contactsWhere = getContactsWhere(searchPattern,withLimit);
        String orderOrientation = getOrientationOrder(searchPattern);
        string subquery = '(SELECT '+procedureFields +' FROM Procedures__r WHERE '+procedureWhere + days + ' ORDER BY ' +searchPattern.sortBy +' '+ orderOrientation + ')';
        
        return 'SELECT '+ contactFields +','+ subquery + ' FROM Contact WHERE '+contactsWhere;        
    }

    public static String getDateFormat(Date pDate){
        String month = pDate.month() > 9 ? String.valueOf(pDate.month()):'0'+String.valueOf(pDate.month());
        String day = pDate.day() > 9 ? String.valueOf(pDate.day()):'0'+String.valueOf(pDate.day());
        return pDate.year() + '-' + month + '-' + day;
    }

    public static Integer getValidPassportYears(){
        if(!(Schema.sObjectType.Passport_Expiration__mdt.isAccessible() &&
            Schema.sObjectType.Passport_Expiration__mdt.fields.MasterLabel.isAccessible() &&
            Schema.sObjectType.Passport_Expiration__mdt.fields.Valid_Years__c.isAccessible())) {
        DCException.throwPermiException('dcSearchUtility.getValidPassportYears');
        }

        List<Passport_Expiration__mdt> passportSettings = [SELECT Valid_Years__c FROM Passport_Expiration__mdt WHERE MasterLabel = 'Passport_Valid Years'];
        Integer years = 6;
        if(passportSettings.size() > 0) {
            years = (Integer) passportSettings[0].Valid_Years__c;
        }
        return years;       
    }

    public static void checkAccessible(){
        if(!(Schema.sObjectType.Contact.isAccessible() &&
             Schema.sObjectType.Contact.fields.Id.isAccessible() &&
             Schema.sObjectType.Contact.fields.FirstName.isAccessible() &&
             Schema.sObjectType.Contact.fields.Middle_Name__c.isAccessible() &&
             Schema.sObjectType.Contact.fields.LastName.isAccessible() &&
             Schema.sObjectType.Contact.fields.Second_Last_Name__c.isAccessible() &&
             Schema.sObjectType.Contact.fields.Gender__c.isAccessible() &&
             Schema.sObjectType.Contact.fields.Birthdate.isAccessible() &&
             Schema.sObjectType.Account.isAccessible() &&
             Schema.sObjectType.Account.fields.Id.isAccessible() &&
             Schema.sObjectType.Account.fields.Name.isAccessible() &&
             Schema.sObjectType.Procedure__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Customer__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Name.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.CreatedDate.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.RecordTypeId.isAccessible() &&
             Schema.sObjectType.RecordType.isAccessible() &&
             Schema.sObjectType.RecordType.fields.Name.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Status__c.isAccessible())) { 
            DCException.throwPermiException('dcSearchUtility.checkAccessible');
        }
    }

}