/**
 * @File Name          : dcCityCodes.cls
 * @Description        : contiene los codigos de ciudades para el XML 
 * @Author             : Elizabeth Betancourt
 * @Group              : 
 * @Last Modified On   : 09-28-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/9/2020   Elizabeth Betancourt     Initial Version
**/

public with sharing class dcCityCodes {

     public Map<string, string> cityCodes {get; set;}
    
     public dcCityCodes(){
        cityCodes = new Map<string,string>();
        cityCodes.put('MIAMI LAKES','2764');
        cityCodes.put('MIDLAND','28391');
        cityCodes.put('AUSTIN','3122');
        cityCodes.put('PEYTON','6478');
        cityCodes.put('MIAMI','2677');
        cityCodes.put('RICHMOND','28631');
        cityCodes.put('DALLAS','3120');
        cityCodes.put('CH',''); 
        cityCodes.put('MEMPHIS','3113');
        cityCodes.put('ARLINGTON','27583');
        cityCodes.put('ATLANTA','2774');
        cityCodes.put('AVENTURA','2760');
        cityCodes.put('BRADENTON','2719');
        cityCodes.put('COLORADO SPRINGS','6260');
        cityCodes.put('HIALEAH','2690');
        cityCodes.put('HOMESTEAD','2694');
        cityCodes.put('DORAL','3251');
        cityCodes.put('HOUSTON','3121');
        cityCodes.put('JAKCON HGTS','0');
        cityCodes.put('KERMIT','28198');
        cityCodes.put('LAS VEGAS','2943');
        cityCodes.put('LEHIGH ACRES','2730');
        cityCodes.put('MCALLEN','28365');
        cityCodes.put('ODESSA','28485');
        cityCodes.put('PLANO','31324');
        cityCodes.put('SAINT JOSEPH','9291');
        cityCodes.put('WEEHAWKEN','2976');
        cityCodes.put('WEST PALM BEACH','2685'); 

     }
    
     public string getCityCode( String city){
        if( cityCodes.containsKey(city) )
            return cityCodes.get(city);
         
        return ''; 
     }
    
    public string getCityCode( String city, string state){
        if(!(Schema.sObjectType.CityCode__mdt.isAccessible() &&
            Schema.sObjectType.CityCode__mdt.fields.Label.isAccessible() &&
            Schema.sObjectType.CityCode__mdt.fields.City__c.isAccessible() &&
            Schema.sObjectType.CityCode__mdt.fields.State__c.isAccessible() &&
            Schema.sObjectType.CityCode__mdt.fields.Code__c.isAccessible())) {
        DCException.throwPermiException('dcCityCodes.getCityCode');
        }
        
        List<CityCode__mdt> mdtList = [SELECT Code__c
                                       FROM CityCode__mdt
                                       WHERE State__c= :state AND City__c= :city];
        if(mdtList.size() > 0) {
            return mdtList[0].Code__c;
        }
        return ''; 
     }
}