<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Web Service Settings</label>
    <protected>false</protected>
    <values>
        <field>Fedex_Account__c</field>
        <value xsi:type="xsd:string">787299229</value>
    </values>
    <values>
        <field>Meter_Number__c</field>
        <value xsi:type="xsd:string">251924635</value>
    </values>
    <values>
        <field>Payor_AccNumber__c</field>
        <value xsi:type="xsd:string">787299229</value>
    </values>
    <values>
        <field>Shipper_City__c</field>
        <value xsi:type="xsd:string">Washington</value>
    </values>
    <values>
        <field>Shipper_Company__c</field>
        <value xsi:type="xsd:string">District Cuba INC</value>
    </values>
    <values>
        <field>Shipper_Country_Code__c</field>
        <value xsi:type="xsd:string">US</value>
    </values>
    <values>
        <field>Shipper_Mail__c</field>
        <value xsi:type="xsd:string">rafael@districtcuba.com</value>
    </values>
    <values>
        <field>Shipper_Name__c</field>
        <value xsi:type="xsd:string">Rafael Santurio</value>
    </values>
    <values>
        <field>Shipper_Phone__c</field>
        <value xsi:type="xsd:string">(202) 321-1169</value>
    </values>
    <values>
        <field>Shipper_Postal_Code__c</field>
        <value xsi:type="xsd:string">20009</value>
    </values>
    <values>
        <field>Shipper_State_Code__c</field>
        <value xsi:type="xsd:string">DC</value>
    </values>
    <values>
        <field>Shipper_Street__c</field>
        <value xsi:type="xsd:string">1746 Columbia Rd NW Suite 1</value>
    </values>
    <values>
        <field>Transaction_Id__c</field>
        <value xsi:type="xsd:string">Shipping Request from District Cuba</value>
    </values>
    <values>
        <field>User_Key__c</field>
        <value xsi:type="xsd:string">1GsQ2XLwCuCZP2P4</value>
    </values>
    <values>
        <field>User_Password__c</field>
        <value xsi:type="xsd:string">raRFHsW88s5hdpTGPPPmiNhc9</value>
    </values>
    <values>
        <field>Web_Service_EndPoint__c</field>
        <value xsi:type="xsd:string">https://ws.fedex.com:443/web-services</value>
    </values>
</CustomMetadata>
