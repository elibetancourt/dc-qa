({
	CLOSE_ACTION : 1,
	OK_ACTION : 2,
	CANCEL_ACTION : 3,

	close :  function(cmp, action) {
		var confirmClosedEvent = cmp.getEvent("confirmationClosed"); 
		confirmClosedEvent.setParams({"action" : action }); 
		confirmClosedEvent.fire();
	}
})