({
	init: function(component, event, helper) {
            
            helper.reset(component, helper);
    },
    
    handleDataTableSelect: function(component, event, helper) {
        
        var rows = event.getParam('rows');
        //alert(JSON.stringify(rows));
        //guardar en el componente los valores de fila seleccionada
        component.set("v.selectedRows", rows); 
        helper.changeSelected(component, helper);
    },
    
    handleConfirmationEvent : function(component, event, helper) {
        //alert('entre al evento confirmacion');
        var action = event.getParam("action");
        //alert('@@@ ==> ' + action);
        var showModal = false;
        component.set("v.showConfirmation", showModal);
        //if action = 2 user selecciono OK, ejecutar Pagar Tramite
        if (action == 2){
            //var i=0;
            var itemsToApply = [];
            var rows = component.get("v.selectedRows"); 
            //alert(JSON.stringify(rows));
            Object.values(rows).forEach(row => {               
                itemsToApply.push(row.batch_item_id);
            });
                
            var recordId = component.get("v.id");
            var itemsToApplyJSON = JSON.stringify(itemsToApply);
            //alert(itemsToApplyJSON);   
            var item = {batchId: recordId, itemsToApply: itemsToApply};
            var itemsPattern = {itemsPattern: JSON.stringify(item)};
            //alert(JSON.stringify(itemsPattern)) ;                          
            helper
            .runServerAction(component, helper, "applyPayments", itemsPattern)
            .then(function(result) {
                 alert(result);
               
                 if (result === 'OK') {
                     //alert('estoy en el if de toast message');
                     var message = component.get("v.successmessage");
                     var title = component.get("v.title");
                     //alert(message);
                     helper.showToast(component, title, message, "info");
                    //hacer un reset de la pagina 
                     helper.reset(component, helper);                        
                 }
                 else{
                     var message = component.get("v.serverErrorMessage");
                     var title = component.get("v.title");
                     helper.showToast(component, title, message, "error");                  
                 }                  
            });
        }            
    },
    
    applyPayment: function(component, event, helper) {
        //  activar el modal
        
        var showModal = true;
        component.set("v.showConfirmation", showModal);
    }
})