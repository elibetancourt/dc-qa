({
	init: function(component, event, helper) {
        var pdfPage = component.get("v.pdfPage");
        var idManifest = component.get("v.idManifest");
        var idProcedure = component.get("v.idProcedure");
        
        var id = (idManifest ? idManifest : idProcedure);
        //alert(id);
        //alert(idManifest);
        
		var redirect = pdfPage+"?id="+id;
        component.set("v.pdfPageWithId", redirect);
        //alert(redirect);
    },     
    closeModal : function(component, event, helper){
        var cmpTarget = component.find('modalbox');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
    }

})